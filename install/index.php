<?php
include_once 'common.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>安装程序 -smallshop</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/public/js/jquery.js"></script>
</head>

<body>

<div class="top">
    <div class="top-logo">
        <img src="images/top-logo.png" height="70">
    </div>
    <div class="top-link">
        <ul>
            <li><a href="http://www.smallshop.com" target="_blank">官方网站</a></li>
            <li><a href="#" target="_blank">QQ群:322257814</a></li>
        </ul>
    </div>
    <div class="top-version">
        <!-- 版本信息 -->
        <h2>smallshop</h2>
    </div>
</div>

<div class="main">
    <div class="pleft">
        <dl class="setpbox t1">
            <dt>安装步骤</dt>
            <dd>
                <ul>
                    <li class="now">许可协议</li>
                    <li>环境检测</li>
                    <li>参数配置</li>
                    <li>正在安装</li>
                    <li>安装完成</li>
                </ul>
            </dd>
        </dl>
    </div>
    <div class="pright">
        <div class="menter_lf"><span>许可协议</span></div>
        <div class="pr-agreement">
            <?php
            $license_file = dirname(__FILE__) . '/../license.txt';
            if (file_exists($license_file)) {
                $license_data = file($license_file);
                foreach ($license_data as $key) {
                    echo $key . '<br>';
                }
            } else {
                echo "本地协议不存在，请访问： <a href='http://www.smallshop.me/article/34.html' target='_blank'>http://www.smallshop.me/article/34.html</a> 阅读相关协议";
            }
            ?>
        </div>
        <div class="btn-box">
            <input name="readpact" type="checkbox" id="readpact" value="" class="check_boxId"/>
            <label for="readpact"><strong class="fc-690 fs-14">我已经阅读并同意此协议</strong></label>
            <input name="继续" type="submit" class="menter_btn_a_a_lf" value="继续"/>
        </div>
    </div>
</div>

<div class="foot">

</div>
<script language="JavaScript">
    $(document).ready(function (e) {
        $(".menter_btn_a_a_lf").click(function () {
            if ($(".check_boxId").is(":checked")) {

                window.location.href = "install2.php";
            }
            else {
                alert("请同意安装协议");
            }
        });
    });
</script>
</body>
</html>
