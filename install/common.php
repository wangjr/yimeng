<?php
error_reporting(0);
if (file_exists(dirname(__FILE__) . '/install.lock')) {
    header('location:/index.php');
    exit;
}

function write_dir($dir)
{
    $writeable = "0";
    if (!is_dir($dir)) {
        @mkdir($dir, 0777);
    }
    if (is_dir($dir)) {
        if ($fp = @fopen("$dir/index.html", 'w')) {
            @fclose($fp);
            $writeable = "1";
        }
    }
    return $writeable;
}

function error_json($info = '信息错误', $status = 'n')
{
    if ($info == 'y') {
        exit(ch_json_encode(array('status' => 'y')));
    } else {
        if (is_array($info)) {
            $status = 'y';
            if (!empty($info['status'])) {
                $status = $info['status'];
                unset($info['status']);
            }
            $res = array(
                'result' => $info,
                'status' => $status,
            );
            exit(ch_json_encode($res));
        } else {
            exit(ch_json_encode(array('info' => $info, 'status' => $status)));
        }
    }
}

function ch_json_encode($data)
{
    if (version_compare(phpversion(), '5.4.0') >= 0) {
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }

    $result = '';
    $result = json_encode($data);
    //对于中文的转换
    return preg_replace("#\\\u([0-9a-f]{4})#ie", "iconv('UCS-2BE', 'UTF-8', pack('H4', '\\1'))", $result);
}

?>